package week3;

import java.util.Arrays;
import java.util.Scanner;



public class Main {

    public static boolean checkName( String name ) {
        return name.matches( "[A-Z][a-z]*" );
    }


    private static boolean checkYear(String[] arr, String toCheckValue)
    {
        // sort given array
        Arrays.sort(arr);

        // check if the specified element
        // is present in the array or not
        // using Binary Search method
        int res = Arrays.binarySearch(arr, toCheckValue);

        boolean test = res > 0 ? true : false;

        return test;
    }

    public static void main(String[] args) {

        Student stud = new Student();
        // create an object of Scanner
        Scanner input = new Scanner(System.in);

        String name;
        boolean validName = false;
        // ask student for their information
        do {
            System.out.println("Please enter your name:");
            name = input.next();
            if(checkName(name)){
                validName = true;
            }else{
                System.out.println("Needs to have letters as valid input");
                validName = false;
            }
        }while(!(checkName(name)));
        stud.setName(name);

        String grade = "";
        Boolean validGrade= false;
        String[] grades = {"Senior", "Junior", "Sophomore", "Freshman", "sen","jun","soph","fresh",};

        do {
            System.out.println("Please enter your year: (ie 'Senior', 'Junior', 'Sophomore', 'Freshman' ) ");
            grade = input.next();
            if (checkYear(grades,grade)){
                validGrade = true;
            }else{
                System.out.println("you can type \"sen\",\"jun\",\"soph\",\"fresh\" for short :)  ");
               validGrade = false;
            }
        }while (!(validGrade));
        stud.setGrade(grade);



        boolean validID;
        int idnumber = 0;
        int length;

        do {
            System.out.println("Please enter your 6-digit ID number:");
            if (input.hasNextInt()) {
                idnumber = input.nextInt();
                length = String.valueOf(idnumber).length();
                if (length == 6){
                    validID = true;
                }else{
                    System.out.println("must be 6 digits");
                    validID= false;
                }
            } else {
                System.out.println("only use numbers [0-9]");
                validID= false;
                input.next();
            }
        }while (!(validID));
        stud.setIDnumber(idnumber);

        System.out.println("\nUsing a class that converts student info to JSON object");
        String json = JSONExample.studentToJSON(stud);
        System.out.println(json);

        System.out.println("\nUsing a class that converts JSON object back to student info");
        Student stud2 = JSONExample.JSONToStudent(json);
        System.out.println(stud2);
    }
}
