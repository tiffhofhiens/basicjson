package week3;

public class Student {

    private String name;
    private String grade;
    private int idnumber;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public int getIDnumber() {
        return idnumber;
    }

    public void setIDnumber(int idnumber) {
        this.idnumber = idnumber;
    }

    public String toString() {
        return "Name: " + name + " | Grade: " + grade +" | Id Number: " + idnumber;
    }
}
