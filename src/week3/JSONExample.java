package week3;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

public class JSONExample {

    public static String studentToJSON(Student student) {

        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(student);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return s;
    }

    public static Student JSONToStudent(String s) {

        ObjectMapper mapper = new ObjectMapper();
        Student student = null;

        try {
            student = mapper.readValue(s, Student.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return student;
    }

}
